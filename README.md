# Warp Breakout

[![build status](https://gitlab.com/kalouantonis/warp-breakout/badges/master/build.svg)](https://gitlab.com/kalouantonis/warp-breakout/commits/master)

A breakout implementation with time travel.

## Requirements 
* Java 8
* Boot

## Running the project

Start the development server:
```
$ boot dev
```

Connect the emacs REPL by using `M-x cider-connect` and running `(start-repl)`.

## Running the tests

Run all tests once:

```
$ boot test
```

Run tests on file change:

```
$ boot auto-test
```

## License

Copyright © 2016 Antonis Kalou

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
