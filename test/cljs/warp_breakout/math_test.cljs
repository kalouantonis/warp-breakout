;; Math test? Oh dear!
(ns warp-breakout.math-test
  (:require-macros
   [cljs.test :refer [deftest testing is are]])
  (:require
   [warp-breakout.math :as math]
   [cljs.test :as t]))

(deftest test-clamp []
  (are [x y] =
    3 (math/clamp 3 0 5)
    8 (math/clamp -10 8 25)
    20 (math/clamp 100 1 20)))

(deftest test-circle->rect
  (is (= (math/circle->rect {:x 10 :y 10 :radius 5})
         {:x 5 :y 5 :width 10 :height 10})))

(deftest test-point-in-rect? []
  (let  [rect {:x 10 :y 5 :width 32 :height 16}]
    (testing "point in center of rect"
      (is (math/point-in-rect? {:x (+ (:x rect) (/ (:width rect) 2))
                                :y (+ (:y rect) (/ (:height rect) 2))}
                               rect)))
    (testing "point at edge of x"
      (are [p rect] (math/point-in-rect? p rect)
        {:x (:x rect) :y (:y rect)} rect
        {:x (:width rect) :y (:height rect)} rect))
    (testing "point outside of rect"
      (are [p rect] (not (math/point-in-rect? p rect))
        {:x 9 :y 5} rect
        {:x 11 :y 4} rect
        {:x 43 :y 12} rect
        {:x 22 :y 22} rect
        {:x 0 :y 0} rect
        {:x 43 :y 22} rect))))

(deftest test-intersection []
  (testing "no intersection"
    (are [m] (not-any? true? m)
      (math/intersection {:x 20 :y 10 :width 50 :height 50}
                         {:x 0 :y 0 :width 15 :height 5})
      (math/intersection {:x 0 :y 0 :width 40 :height 25}
                         {:x 100 :y 27 :width 20 :height 20})))
  (testing "partial intersection"
    (are [k rects] (k (apply math/intersection rects))
      :top? [{:x 20 :y 10 :width 50 :height 50}
             {:x 10 :y 5 :width 50 :height 25}]
      :bottom? [{:x 20 :y 45 :width 50 :height 15}
                {:x 20 :y 10 :width 50 :height 50}]
      :left? [{:x 10 :y 10 :width 15 :height 23}
              {:x 20 :y 10 :width 25 :height 23}]
      :right? [{:x 45 :y 10 :width 20 :height 23}
               {:x 50 :y 10 :width 60 :height 23}]))
  (testing "intersection on multiple sides"))

;; Property-based testing (WIP)
(comment
  (def gen-pos-double
    "Generate a positive double value."
    (gen/double* {:min 0}))

  (def gen-point
    "Generate a point in euclidean space."
    (gen/hash-map
     :x gen/double
     :y gen/double))

  (def gen-point-within
    (gen/bind gen-point ))

  (defn gen-point-within
    [{:keys [x y width height]}]

    (gen/hash-map
     :x (gen/double* {:min x :max (+ x width)})
     :y (gen/double* {:min y})))

  (defn gen-point-outside
    [{:keys [x y width height]}])

  (def gen-rect
    "Generate a rectangle."
    (gen/hash-map
     :x gen/double
     :y gen/double
     :width gen-pos-double
     :height gen-pos-double))

  (defspec point-in-rect-is-false-outside-bounds
    100
    (prop/for-all [p gen-point
                   rect gen-rect]
      (not (math/point-in-rect? p rect))))

  (defspec point-in-rect-is-true-inside-bounds
    100
    (prop/for-all [rect gen-rect
                   p (gen-point-within rect)]
      (math/point-in-rect? p rect)))
  )

