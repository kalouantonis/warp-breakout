(ns warp-breakout.app
  "warp-breakout entry point. A simple game that
  mixes the mechanics of breakout and 2048."
  (:require
   [cljs.core.async :as async :refer [<! >!]]
   [goog.events :as gevents]
   [goog.events.EventType :as event-type]
   [goog.Timer]
   [warp-breakout.draw :as draw]
   [warp-breakout.events :as events]
   [warp-breakout.math :as math]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Config
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def debug?
  "Signifies if the current build is in debug mode."
  ^boolean js/goog.DEBUG)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Updating
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TODO: Account for skipped frames and apply logic
(defn start-update-loop!
  "Start a timed update loop that will run every timestep ms.
  The update loop will compose fns, a list of functions, and call
  them with the current state and delta-time."
  [state timestep fns]
  (let [timer (goog.Timer. timestep)
        update-fn (apply comp fns)
        tick (fn []
               (swap! state update-fn (/ timestep 1000)))]
    (gevents/listen timer goog.Timer/TICK tick)
    (swap! state assoc :timer timer) ;; Add timer to state
    (when-not (.-enabled timer)
      (.start timer))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Game logic
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def game-width 500)
(def game-height 400)
(def fps 60)

(def paddle {:x (- (/ game-width 2) 50) :y (- game-height 20)
             :width 100 :height 15 :color "#00ff00"})

(def ball {:x 100 :y 200 :vx 8 :vy 16
           ;; Speed multiplier in both X and Y directions
           :speed 15 :max-speed 23
           :radius 7 :color "#ff0000"})

(def brick {:width 50 :height 15 :color "#00ff00"})

(defn render!
  "Render game state. Nothing is returned, as render never
  modifies the game data."
  [ctx {:keys [paddle ball bricks score]}]
  (draw/clear! ctx "#000000")
  (draw/circle! ctx ball (:color ball))
  (draw/rectangle! ctx paddle (:color paddle))
  (doseq [brick bricks]
    (draw/rectangle! ctx brick (:color brick)))
  (let [{:keys [val x y]} score]
    (set! (.-fillStyle ctx) "#ff0000")
    (set! (.-font ctx) "24px serif")
    (.fillText ctx val x y)))

(defn move
  "Move game objects."
  [state dt]
  (let [{:keys [x y vx vy speed] :as ball} (:ball state)]
    (update state :ball #(merge % {:x (+ (* vx speed dt) x)
                                   :y (+ (* vy speed dt) y)}))))

(defn deflect
  "Deflect at the ball according to the hit distance from the
  center of the paddle."
  [ball paddle closest-p]
  (let [vx (* (:speed ball)
              (/ (- (:x closest-p)
                    (+ (:x paddle) (/ (:width paddle) 2)))
                 (/ (:width paddle) 2)))]
    (merge ball {:vy (- (:vy ball)) :vx vx})))

(defn collide-paddle
  [{:keys [paddle ball] :as state}]
  (let [intersection (math/circle-rect-intersection ball paddle)]
    (if (and intersection (> (:vy ball) 0))
      (update state :ball deflect paddle (:closest intersection))
      state)))

(defn increase-decayed
  "Increase speed by a multiplier of mul, up to speed-max.
  This is a decayed increase, so speed increases less that
  higher the speed is."
  [mul speed max-speed]
  (let [decay (- 1 (/ speed max-speed))]
    (+ speed (* mul decay))))

(defn collide-brick
  "Handle brick collisions."
  [{:keys [ball bricks score] :as state}]
  (let [colliding (-> #(math/circle-rect-intersection ball %)
                      (filter bricks)
                      (set))]
    (if (seq colliding)
      (let [ball-rect (math/circle->rect ball)
            ;; Get collision points for bricks that are definetally
            ;; collided with.
            intersections (map #(math/intersection ball-rect %) colliding)
            collision-points (reduce (fn [acc [k v]]
                                       (if (true? v)
                                         (conj acc k)
                                         acc))
                                     [] (mapcat vec intersections))
            intesect-x? (some #{:left? :right?} collision-points)
            intesect-y? (some #{:top? :bottom?} collision-points)]
        (println collision-points)
        (merge state {:ball (merge ball {:vx (if intesect-x?
                                               (- (:vx ball))
                                               (:vx ball))
                                         :vy (if intesect-y?
                                               (- (:vy ball))
                                               (:vy ball))})
                      :speed (increase-decayed
                              10 (:speed ball) (:max-speed ball))
                      :bricks (remove #(contains? colliding %) bricks)
                      :score (update score :val + (* 10 (count colliding)))}))
      state)))

(defn collide
  "Handle object collisions."
  [{:keys [ball paddle] :as state} dt]
  (let [{:keys [radius x y]} ball]
    (cond
      (or (> x (- game-width radius))
          (< x radius))
      (update-in state [:ball :vx] unchecked-negate)
      (or (> y (- game-height radius))
          (< y radius))
      (update-in state [:ball :vy] unchecked-negate)
      :else (collide-brick (collide-paddle state)))))

(events/reg-event
 :mousemove
 (fn [{:keys [paddle] :as state} ev]
   (let [x (.-clientX ev)
         width (:width paddle)]
     (if (and (>= (- x (/ width 2)) 0)
              (<= (+ x (/ width 2)) game-width))
       (assoc-in state [:paddle :x] (- (.-clientX ev)
                                       (/ width 2)))
       state))))

;; Events helpful for debugging
(when debug?
  ;; Stop time (ball will not move)
  (events/reg-event
   :click
   (fn [{:keys [timer] :as state} ev]
     (if (.-enabled timer)
       (.stop timer)
       (.start timer))
     state)))

(defn ^:export init []
  (enable-console-print!)
  (let [canvas (.querySelector js/document "canvas")
        bricks (for [x (range 0 game-width (inc (:width brick)))
                     y (range 50 (/ game-height 3) (inc (:height brick)))]
                 (merge brick {:x (inc x)
                               :y (inc y)}))
        state (atom {:paddle paddle :ball ball :bricks bricks
                     :score {:val 0 :x 10 :y 25}})]
    (set! (.-width canvas) game-width)
    (set! (.-height canvas) game-height)
    ;; Setup event handling loop
    (events/start-event-handler! state canvas)
    ;; Update loop
    (start-update-loop! state (/ 1000 fps) [collide move])
    ;; Render loop
    ;; TODO: Make loops reloadable (currently loops never exit)
    ((fn this [timestep]
       (render! (.getContext canvas "2d") @state)
       (js/requestAnimationFrame this)))))
