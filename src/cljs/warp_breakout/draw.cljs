(ns warp-breakout.draw
  "Rendering utilities for drawing basic primitives.")

(defn rectangle!
  "Render a rectangle with a certain colour string."
  [ctx {:keys [x y width height] :as rect} color]
  (set! (.-fillStyle ctx) color)
  (.fillRect ctx x y width height))

(defn circle!
  "Render a circle with a certain colour string."
  [ctx {:keys [x y radius]} color]
  (set! (.-fillStyle ctx) color)
  (doto ctx
    (.beginPath)
    (.arc x y radius 0 (* 2 Math/PI))
    (.closePath)
    (.fill)))

(defn clear!
  "Set the whole game screen to a certain colour."
  [ctx color]
  (rectangle! ctx {:x 0 :y 0
                   :width (.-width (.-canvas ctx))
                   :height (.-height (.-canvas ctx))}
              color))
