(ns warp-breakout.math
  "Reusable math utilities.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn abs
  [x]
  (.abs js/Math x))

(defn clamp
  "Ensure that the value of x is between min and max."
  [x min max]
  (cond
    (< x min) min
    (> x max) max
    :else x))

(defn square
  "Returns x to the power of 2."
  [x]
  (.pow js/Math x 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Geometry
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn circle->rect
  "Convert a circle object in to a rectangle."
  [{:keys [x y radius]}]
  (let [d (* radius 2)]
    ;; Rectangles use bottom left as x, y when rendering,
    ;; whereas circles use center x, y.
    {:x (- x radius), :y (- y radius)
     :width d, :height d}))

(defn point-in-rect?
  "Returns true if a point is is withing a rectangle."
  [p {:keys [x y width height]}]
  (and (<= x (:x p) (+ x width))
       (<= y (:y p) (+ y height))))

(defn intersection
  "Returns a map indicating collision points between
  two rectangles."
  [{x1 :x y1 :y w1 :width h1 :height}
   {x2 :x y2 :y w2 :width h2 :height}]
  (let [hit-points {:top? false, :bottom? false
                    :left? false, :right? false}
        w (/ (+ w1 w2) 2)
        h (/ (+ h1 h2) 2)
        dx (- (+ x1 (/ w1 2))
              (+ x2 (/ w2 2)))
        dy (- (+ (/ h1 2) y1)
              (+ (/ h2 2) y2))]
    (if (and (> w (abs dx)) (> h (abs dy)))
      (let [wy (* w dy)
            hx (* h dx)]
        {:top?    (and (> wy hx) (> wy (- hx)))
         :left?   (and (> wy hx) (<= wy (- hx)))
         :right?  (and (<= wy hx) (> wy (- hx)))
         :bottom? (and (<= wy hx) (<= wy (- hx)))})
      hit-points)))

(defn intersects?
  "Returns true if two rectangles interlap in euclidean space.
  Specifically checks that the first argument is contained in
  the second."
  [r1 r2]
  (let [any? (comp boolean some)]
    (any? true? (vals (intersection r1 r2)))))

(defn circle-rect-intersection
  "Returns a map of the closest X and Y points
  if a circle intersects with a rectangle."
  [c r]
  (let [closest-x (clamp (:x c) (:x r) (+ (:x r) (:width r)))
        closest-y (clamp (:y c) (:y r) (+ (:y r) (:height r)))
        dist-x (- (:x c) closest-x)
        dist-y (- (:y c) closest-y)
        dist-sqr (+ (square dist-x) (square dist-y))]
    (if (< dist-sqr (square (:radius c)))
      {:closest {:x closest-x, :y closest-y}}
      nil)))
