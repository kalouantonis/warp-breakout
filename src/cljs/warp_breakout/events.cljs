(ns warp-breakout.events
  "Interface to browser events.")

(def ^:private event-handlers (atom {}))

(defn reg-event
  "Register a new event handler. This must be called
  before start-event-handler!. "
  [kw-name f]
  (swap! event-handlers assoc kw-name f))

;; TODO: Make it so that when event-handlers is changed,
;; (removed/added) the js/addEventListener is updated.
(defn start-event-handler!
  "Start the event handler."
  [state canvas]
  (letfn [(add-event-listener [kw-name f]
            (.addEventListener canvas
              (name kw-name)
              (fn [ev]
                (swap! state f ev))))]
    ;; Add for current state
    (doseq [[kw-name f] @event-handlers]
      (add-event-listener kw-name f))))
